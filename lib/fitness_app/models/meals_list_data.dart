class MealsListData {
  MealsListData({
    this.imagePath = '',
    this.titleTxt = '',
    this.startColor = '',
    this.endColor = '',
    this.meals,
    this.kacl = 0,
    this.hour = '',
  });

  String imagePath;
  String titleTxt;
  String startColor;
  String endColor;
  List<String> meals;
  int kacl;
  String hour;

  static List<MealsListData> tabIconsList = <MealsListData>[
    MealsListData(
      imagePath: 'assets/fitness_app/water.png',
      titleTxt: '',
      kacl: 602,
      meals: <String>['XV de Novembro', 'Centro,', 'Faculdade'],
      startColor: '#738AE6',
      endColor: '#738AE6',
      hour: '08:26',
    ),
    MealsListData(
      imagePath: 'assets/fitness_app/fire.png',
      titleTxt: '',
      kacl: 525,
      meals: <String>['Saldanha', 'Centro,', 'Lago'],
      startColor: '#FA7D82',
      endColor: '#FA7D82',
      hour: '10:30',
    ),
    MealsListData(
      imagePath: 'assets/fitness_app/stone.png',
      titleTxt: '',
      kacl: 0,
      meals: <String>['Pedro Carli,', 'Vila Carli,', 'Proximo a 277'],
      startColor: '#D2691E',
      endColor: '#D2691E',
      hour: '21:56',
    ),
  ];
}
